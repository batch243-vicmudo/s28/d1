// CRUD Operations
/*
	-CRUD operations are the heart of any backend application.
	-mastering the CRUD operations is very essential for any developer.
	-This helps in building character and increasing exposure to logical statements that will help us manipulate.
	-mastering the CRUD operations of any languages makes us a valuable developer and makes the work easier for us to deal with hure amounts of information.
*/
// [SECTION] Inserting document (Create)

	// Insert one document
	/*
		-since MongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
		-mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code.
		Syntax:
			db.collectionName.insertOne({onject});
	*/

db.users.insert({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact:{
			phone: "12345678",
			email: "jandedoe@gmail.com"
		},
		courses: [ "CSS", "JavaScript", "Python"],
		department: "none"
	})

// Insert Many
	/*
		-Syntax
		db.collectionName.insertMany([{ObjectA}, {OnjectB}])
	*/

	db.users.insertMany([
		{firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact:{
			phone: "8765321",
			email: "stephenhawking@gmail.com"
		},
		courses: [ "Python", "React" "PHP"],
		department: "none"
	}, {
			firstName: "Neit",
		lastName: "Armstrong",
		age: 82,
		contact:{
			phone: "15343378",
			email: "neilaemstong@gmail.com"
		},
		courses: [ "React", "Laravel", "SASS"],
		department: "none"}])

	//[Section] Finding Documents(read)
	//Find
	/*
		-if multiple documents match the criteria for finding a document only the first document that matches the search term will be returned
		-this is based from the order that the documents are strored in collection

		Syntax:
			db.collectionName.find();
			db.collectionName.find({field: value});
	*/

	db.users.find();

	db.users.find({firstName : "Stephen"});
	// pretty method
	// The pretty method allows us to be able to view the documents returned by our terminal in a better format.

	db.users.find({firstName : "Stephen"});.pretty();

	// Finding documents with multiple parameters
	/*
		-Syntax:
		db.collectionName.find({fieldA : valueA, fieldB : valueB})
	*/

	db.users.find({lastName: "Armstrong", age: 82}).pretty();

	// [Section] Updating documents

	//Updating a single document
	db.users.insert({
		firstName: "test",
		lastName: "test",
		age: 0,
		contact:{
			phone: "000000000",
			email: "test@gmail.com"
		},
		courses: [],
		department: "none"
	})

	/*
		-just like the find method, updateOne method will only manipulate a single document. First document that matches the search criteria will be updated.

		Syntax:
			db.collectionName.updateOne ({criteria}, { $set: {field:value}})
	*/

	db.users.updateOne({ firstName: "Test" },{$set: {firstName: "Bill",},});

	db.users.updateOne(
		{ firstName: "Bill"},
		{
			$set:{
				lastName: "Gates",
				age: 65,
				contact: {
					phone: "12345678",
					email: "bill@gmail.com"
				},
				course: ["PHP", "Laravel", "HTML"],
				department: "Operations",
				status: "active"
			}
		}
	);

	//Update multiple documents
	/*
		db.collectionName.updateMany({criteria}, {$set: {objectA}})
	*/

	db.users.updateMany({department : "none"}, {

		$set: {
			department: "HR"
		}
		})

	//Replace One
	//can be used if replacing the whole document necessary
	/*
		Syntax:
		db.collectionName.replaceOne({criteria}, {objectA})
	*/

	db.users.replaceOne({firstName : "Bill")},
		{
			
			firstName : "Chris",
			lastName : "Mortel",
			age : 14,
			contact: {
				phone: "1232435",
				email: "chris@gmail.com"
			},
			course : ["PHP", "Laravel", "HTML"],
			department : "Operations"
			
		})
// [Section] Deleting documents (delete)

db.users.insert({
	firstName: "test"
})

//Deleting a single document
/*
	Syntax:
	db.collectionName.deleteOne({criteria})
*/

	db.users.deleteOne({firstName  : "test"})

//Delete Many
/*
	-be careful when using the deleteMany method. If no search criteria is provided it will delete all documents in the collection.
	- DO NOT USE: db.collectionName.deleteMany();
	Syntax:
	db.collectionName
*/
	db.users.deleteMany ({firstName: "Chris"});

	//[Section] Advanced queries
	//Query an embedded document
	/*
	
	*/

	db.users.find({
		contact: {
			phone: "8765321"
			email: "stephenhawking@gmail.com"
		}
	})

	//query on nested field

	db.users.find({ "contact.phone" : "8765321"})

	//querying an array with exact elements

	db.users.find ({courses: ["Python", "React", "PHP"]})

	// querying an array without regards to order and elements

	db.users.find({courses: {$all: ["React"]}})